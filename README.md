# Introduction

Welcome to my code repository! Here you will find a collection of my distinguished coding projects. I have carefully curated this repository to showcase my skills, experience, and passion for coding.

## About Me

My name is Mihail Dimoski, and I am a dedicated and detail-oriented Front-End Developer with a strong track record of delivering high-quality products on time. With a strong background in JavaScript, I have gained extensive experience in TypeScript, React, Next.js, MUI and Bootstrap. My passion for creating exceptional user experiences drives me to continuously learn and stay up-to-date with the latest industry trends and technologies. Additionally, my communication abilities and people skills enable me to collaborate effectively with cross-functional teams, translating complex technical concepts into clear and concise information.
I thrive in fast-paced agile environments and embrace challenges, always seeking innovative solutions to deliver outstanding results.

## What You'll Find Here

This repository serves as a portfolio of my work, highlighting various projects that I have completed. Each project represents a unique challenge and demonstrates my ability to analyze requirements, design robust solutions, and implement clean and maintainable code.

### Projects:

1.  Project Name: Open OTR

    - Technologies: Next.js, TypeScript, Material-UI, Sass

      **Live development version @ https://otr-no-trucks-parked.vercel.app/**

2.  Project Name: Restaurant App

    - Technologies: React, Typescript, Bootstrap

3.  Project Name: Bike Shop

    - Technologies: React, Typescript, Bootstrap

4.  Project Name: Street ARTists (Mobile Only)

    - Technologies: Vanilla JS, Bootstrap

5.  Project Name: Budget Calculator

    - Technologies: Vanilla JS, Bootstrap

6.  Project Name: Keyboard App

    - Technologies: Vanilla JS, Bootstrap

## How to Navigate

To explore my projects, please switch to the individual branches associated with each project. The main branch of this repository is intentionally left empty, serving as a clean slate for navigation and choosing specific projects of interest.

Within each project branch, you will find a detailed README.md file that provides an overview of the project, its objectives, and the technologies or tools utilized. In addition, the codebase is meticulously organized and structured to optimize readability and understanding.

## Contact Me

If you have any further questions or need more information, please feel free to reach out. I'm keen on contributing my skills to solve challenging problems. My contact information is as follows:

Email: **mihaildimoski@gmail.com**

Phone: **+389 71 332 559**

I appreciate your consideration and would be delighted to further showcase my capabilities.

Sincerely,

Mihail D.
